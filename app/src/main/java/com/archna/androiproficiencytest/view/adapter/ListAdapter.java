package com.archna.androiproficiencytest.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.archna.androiproficiencytest.R;
import com.archna.androiproficiencytest.database.entity.RawEntity;
import com.archna.androiproficiencytest.databinding.RawItemBinding;
import com.archna.androiproficiencytest.util.interfaces.OnItemClick;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private List<RawEntity> _list;
    private OnItemClick<RawEntity> _onIemClick;

    // default constructor
    public ListAdapter(OnItemClick<RawEntity> _onIemClick) {
        this._onIemClick = _onIemClick;
    }

    // for setting the list to adapter
    public void setList(List<RawEntity> list) {
        this._list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RawItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.raw_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ListAdapter.ViewHolder holder, int position) {
        // set rawEntity for the position
        holder.binding.setData(_list.get(position));
    }

    @Override
    public int getItemCount() {
        return _list != null && _list.size() > 0 ? _list.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private RawItemBinding binding;

        ViewHolder(@NonNull RawItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            // Whole item click
            binding.getRoot().setOnClickListener((v) -> {
                int pos = getAdapterPosition();
                _onIemClick.onCLick(_list.get(pos), pos);
            });
        }
    }
}
