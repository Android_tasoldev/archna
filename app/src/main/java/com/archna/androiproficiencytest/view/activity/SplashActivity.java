package com.archna.androiproficiencytest.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.transition.ChangeBounds;
import android.transition.TransitionManager;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.databinding.DataBindingUtil;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import androidx.lifecycle.ViewModelProviders;

import com.archna.androiproficiencytest.R;
import com.archna.androiproficiencytest.database.entity.ItemEntity;
import com.archna.androiproficiencytest.databinding.ActivitySplashBinding;
import com.archna.androiproficiencytest.network.ApiRequest;
import com.archna.androiproficiencytest.network.NetworkObserver;
import com.archna.androiproficiencytest.util.Constants;
import com.archna.androiproficiencytest.util.interfaces.DatabaseCallBack;
import com.archna.androiproficiencytest.viewmodel.MainViewModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private ActivitySplashBinding _binding;
    private MainViewModel _vmMain;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // init data binding
        _binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        // init viewModel
        _vmMain = ViewModelProviders.of(SplashActivity.this).get(MainViewModel.class);

        setInitialDelay();
    }

    // Add some holt before main screen
    private void setInitialDelay() {
        new Handler().postDelayed(this::checkAndCallApi, 1100);
    }

    private void checkAndCallApi() {
        _vmMain.getStoredItemCount(new DatabaseCallBack<Integer>() {
            @Override
            public void onSuccess(Integer item) {
                if (item > 0) {
                    // No need to call api again
                    // Ui thread for execution from different thread
                    runOnUiThread(() -> openList());
                } else {
                    // Need to call api
                    runOnUiThread(() -> callingApi());
                }
            }

            @Override
            public void onFailure() {
                // Need to call api
                runOnUiThread(() -> callingApi());
            }
        });
    }

    private void callingApi() {
        // Show progress view
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this, R.layout.activity_splash_constraint);
        ChangeBounds transition = new ChangeBounds();
        transition.setInterpolator(new FastOutSlowInInterpolator());
        transition.setDuration(800);
        TransitionManager.beginDelayedTransition(_binding.clRoot, transition);
        constraintSet.applyTo(_binding.clRoot);

        // calling api
        if (NetworkObserver.isInternetConnectionAvailable(SplashActivity.this)) {
            new ApiRequest(SplashActivity.this).callApi(Constants.API_GET_ITEM, new Callback<ItemEntity>() {
                @Override
                public void onResponse(@NonNull Call<ItemEntity> call, @NonNull Response<ItemEntity> response) {
                    if (response.body() != null && response.body().getRows() != null) {
                        // Add items to database
                        _vmMain.setItems(response.body(), new DatabaseCallBack<Long>() {
                            @Override
                            public void onSuccess(Long item) {
                                // successfully inserted to db
                                openList();
                            }

                            @Override
                            public void onFailure() {
                                // Fail to insert data
                                _binding.progressBar.setVisibility(View.GONE);
                                _binding.textView.setText(getResources().getString(R.string.please_try_again));
                            }
                        });
                    } else {
                        // body is null
                        _binding.progressBar.setVisibility(View.GONE);
                        _binding.textView.setText(getResources().getString(R.string.please_try_again));
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ItemEntity> call, @NonNull Throwable t) {
                    // Hiding progress and show error message
                    _binding.progressBar.setVisibility(View.GONE);
                    _binding.textView.setText(t.getLocalizedMessage());
                }
            });
        } else {
            // No internet connection available
            _binding.progressBar.setVisibility(View.GONE);
            _binding.textView.setText(getResources().getString(R.string.msg_no_internet));
        }
    }

    // Starting main activity for displaying list
    private void openList() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }
}