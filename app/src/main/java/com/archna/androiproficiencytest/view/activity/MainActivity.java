package com.archna.androiproficiencytest.view.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.widget.Toast;

import com.archna.androiproficiencytest.R;
import com.archna.androiproficiencytest.database.entity.ItemEntity;
import com.archna.androiproficiencytest.databinding.ActivityMainBinding;
import com.archna.androiproficiencytest.network.ApiRequest;
import com.archna.androiproficiencytest.network.NetworkObserver;
import com.archna.androiproficiencytest.util.Constants;
import com.archna.androiproficiencytest.util.Utility;
import com.archna.androiproficiencytest.util.interfaces.DatabaseCallBack;
import com.archna.androiproficiencytest.view.adapter.ListAdapter;
import com.archna.androiproficiencytest.viewmodel.MainViewModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding _binding;
    private MainViewModel _vmMain;
    public ObservableField<String> _obsViewOp = new ObservableField<>(Constants.CONTENT); // for hide show the recycler view and error message
    private ListAdapter _adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // init data binding
        _binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        // setting activity instance to data binding
        _binding.setActivity(this);
        // init viewModel
        _vmMain = ViewModelProviders.of(MainActivity.this).get(MainViewModel.class);

        // Swipe refresh refresh listener
        _binding.swipeToRefresh.setOnRefreshListener(this::refreshData);

        setRecyclerView();
        subscribeToViewModel();
    }

    // set recycler view and init adaptor
    private void setRecyclerView() {
        _adapter = new ListAdapter((object, position) -> Toast.makeText(this, object.getTitle() + "", Toast.LENGTH_SHORT).show());
        _binding.recItem.setAdapter(_adapter);
    }

    // set observers from viewModel
    private void subscribeToViewModel() {
        _vmMain.getLiveItems().observe(this, itemEntity -> {
            if (itemEntity != null) {
                setToolbarTitle(itemEntity.getTitle());
                _adapter.setList(itemEntity.getRows());
                _obsViewOp.set(Constants.CONTENT);
            } else {
                _obsViewOp.set(Constants.NO_DATA);
            }
        });
    }

    // change the title of toolbar
    private void setToolbarTitle(String title) {
        if (Utility.isValidString(title))
            setTitle(title);
    }

    private void refreshData() {
        _binding.swipeToRefresh.setRefreshing(true);
        // calling api
        if (NetworkObserver.isInternetConnectionAvailable(MainActivity.this)) {
            new ApiRequest(MainActivity.this).callApi(Constants.API_GET_ITEM, new Callback<ItemEntity>() {
                @Override
                public void onResponse(@NonNull Call<ItemEntity> call, @NonNull Response<ItemEntity> response) {
                    if (response.body() != null && response.body().getRows() != null) {
                        // Add items to database
                        _vmMain.refreshData(response.body(), new DatabaseCallBack<Long>() {
                            @Override
                            public void onSuccess(Long item) {
                                // Successfully inserted date in db
                                _binding.swipeToRefresh.setRefreshing(false);
                            }

                            @Override
                            public void onFailure() {
                                // Fail to insert data in db
                                _binding.swipeToRefresh.setRefreshing(false);
                                Toast.makeText(MainActivity.this, getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        //response body is null
                        _binding.swipeToRefresh.setRefreshing(false);
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ItemEntity> call, @NonNull Throwable t) {
                    // Hiding progress and show error message
                    _binding.swipeToRefresh.setRefreshing(false);
                    Toast.makeText(MainActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            // No internet connection available
            _binding.swipeToRefresh.setRefreshing(false);
            Toast.makeText(MainActivity.this, getResources().getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
        }
    }
}