package com.archna.androiproficiencytest.view.activity;

import android.app.Application;
import com.facebook.drawee.backends.pipeline.Fresco;

public class AppClass extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize fresco image loading
        Fresco.initialize(this);
    }

}
