package com.archna.androiproficiencytest.network;


import com.archna.androiproficiencytest.database.entity.ItemEntity;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

    // Get method for fetch json
    @GET("facts.json")
    Call<ItemEntity> getItems();
}