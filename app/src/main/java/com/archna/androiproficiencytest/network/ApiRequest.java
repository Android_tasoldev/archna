package com.archna.androiproficiencytest.network;

import android.content.Context;
import com.archna.androiproficiencytest.database.entity.ItemEntity;
import com.archna.androiproficiencytest.util.Constants;

import retrofit2.Callback;

public class ApiRequest {

    private Context context;

    /**
     * Default constructor
     *
     * @param context c= application context
     */
    public ApiRequest(Context context) {
        this.context = context;
    }


    /**
     * @param status   = int value which define a unique api
     * @param callback = retrofit api response callback
     */
    public void callApi(int status, Callback<ItemEntity> callback) {
        if (status == Constants.API_GET_ITEM)
            getItems(callback);
    }


    // Call api for get Items
    private void getItems(Callback<ItemEntity> callback) {
        RetrofitClientInstance
                .getRetrofitInstance()
                .create(ApiService.class)
                .getItems()
                .enqueue(callback);
    }
}
