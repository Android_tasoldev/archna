package com.archna.androiproficiencytest.network;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.JsonObject;

public class ApiResponse {

    public final Status status;

    @Nullable
    public final Object data;

    @Nullable
    public final JsonObject jsonObject;

    @Nullable
    public final String error;

    private ApiResponse(@NonNull Status status, @Nullable Object data, @Nullable JsonObject jsonObject, @Nullable String error) {
        this.status = status;
        this.data = data;
        this.jsonObject = jsonObject;
        this.error = error;
    }

    public ApiResponse(Status status, @Nullable String error) {
        this.status = status;
        this.data = null;
        this.jsonObject = null;
        this.error = error;
    }

    public static ApiResponse loading() {
        return new ApiResponse(Status.LOADING, null, null, null);
    }

    public static ApiResponse success(@Nullable Object data, @Nullable JsonObject jsonObject) {
        return new ApiResponse(Status.SUCCESS, data, jsonObject, null);
    }

    public static ApiResponse error(@NonNull String error) {
        return new ApiResponse(Status.ERROR, null, null, error);
    }

    public enum Status {
        LOADING,
        SUCCESS,
        ERROR,
        NO_INTERNET,
    }
}