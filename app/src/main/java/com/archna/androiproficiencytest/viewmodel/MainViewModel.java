package com.archna.androiproficiencytest.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.archna.androiproficiencytest.database.entity.ItemEntity;
import com.archna.androiproficiencytest.database.repository.ItemsRepository;
import com.archna.androiproficiencytest.util.interfaces.DatabaseCallBack;

public class MainViewModel extends AndroidViewModel {

    private ItemsRepository _repoItem;

    public MainViewModel(@NonNull Application application) {
        super(application);
        _repoItem = new ItemsRepository(application);
    }

    /**
     * Used for getting the total numbers of items stored in database
     *
     * @param callBack contain response from database
     */
    public void getStoredItemCount(DatabaseCallBack<Integer> callBack) {
        _repoItem.getItemCount(callBack);
    }

    /**
     * insert item operations
     *
     * @param itemEntity = item object
     * @param callBack   =  database response callback
     */
    public void setItems(ItemEntity itemEntity, DatabaseCallBack<Long> callBack) {
        _repoItem.insertItem(itemEntity, callBack);
    }

    /**
     * Getting the live list of items
     */
    public LiveData<ItemEntity> getLiveItems() {
        return _repoItem.getLiveItemData();
    }

    public void refreshData(ItemEntity itemEntity, DatabaseCallBack<Long> databaseCallBack) {
        _repoItem.deleteOldDataAndInsertNewData(itemEntity, databaseCallBack);
    }
}