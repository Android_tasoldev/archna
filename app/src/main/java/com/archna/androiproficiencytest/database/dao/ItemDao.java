package com.archna.androiproficiencytest.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.archna.androiproficiencytest.database.entity.ItemEntity;


/**
 * {@link ItemDao} contain database operations
 */
@Dao
public interface ItemDao {

    /**
     * @return whole items from table
     */
    @Query("SELECT * FROM ITEMS")
    LiveData<ItemEntity> getLiveItems();

    /**
     * For insert single item to database
     * no uniqueness in data so no need to define Conflict strategy
     *
     * @param item object of entity {@link ItemEntity}
     */
    @Insert()
    long insertItem(ItemEntity item);

    /**
     * Delete all items from cart
     */
    @Query("DELETE FROM ITEMS")
    void deleteAllItems();

    /**
     * Return total numbers of items stored in table
     *
     * @return int as count
     */
    @Query("SELECT COUNT() FROM ITEMS")
    int getCount();

}
