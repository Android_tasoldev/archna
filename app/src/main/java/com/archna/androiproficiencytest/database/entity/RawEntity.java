package com.archna.androiproficiencytest.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * This class {@link RawEntity} contain raw items
 * and child of {@link ItemEntity}
 */
@Entity(tableName = "Raw")
public class RawEntity {

    // set as autoGenerate primaryKey
    @PrimaryKey(autoGenerate = true)
    private int id;

    // column name for title which is optional
    @ColumnInfo(name = "title")
    private String title;

    private String description; // Description of the content

    private String imageHref;   // Contain image url

    // Get set methods

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageHref() {
        return imageHref;
    }

    public void setImageHref(String imageHref) {
        this.imageHref = imageHref;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
