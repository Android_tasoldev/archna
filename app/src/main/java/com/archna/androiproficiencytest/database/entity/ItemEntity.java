package com.archna.androiproficiencytest.database.entity;

import androidx.databinding.BaseObservable;
import androidx.room.ColumnInfo;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.List;

/**
 * This class {@link ItemEntity} contain raw items of ITEM table
 */
@Entity(tableName = "Items") // Set the table name
public class ItemEntity extends BaseObservable implements Serializable {

    // set as autoGenerate primaryKey
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "title")
    private String title;

    // serialize name which is the name of key in api response
    @ColumnInfo(name = "rows")
    private List<RawEntity> rows;

    // Set Get methods

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<RawEntity> getRows() {
        return rows;
    }

    public void setRows(List<RawEntity> rows) {
        this.rows = rows;
    }
}
