package com.archna.androiproficiencytest.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.archna.androiproficiencytest.database.dao.ItemDao;
import com.archna.androiproficiencytest.database.entity.ItemEntity;
import com.archna.androiproficiencytest.database.entity.RawEntity;
import com.archna.androiproficiencytest.database.typeconverters.RawConverter;
import com.archna.androiproficiencytest.util.Constants;

@Database(entities = {
        ItemEntity.class,
        RawEntity.class
}, version = Constants.DATABASE_VERSION)
@TypeConverters({RawConverter.class})
public abstract class AppDatabase extends RoomDatabase {

    public static AppDatabase databaseInstance;

    public static AppDatabase getInstance(Context context) {
        if (databaseInstance == null) {
            synchronized (AppDatabase.class) {
                // Creating new instance for app database
                databaseInstance = Room.databaseBuilder(
                        context.getApplicationContext(),
                        AppDatabase.class,
                        Constants.DATABASE_NAME)
                        .fallbackToDestructiveMigration() // this will remove all the previous data if database version is change
                        .build();
            }
        }
        return databaseInstance;
    }

    // For clear all the tables in database
    public void clearDatabase() {
        databaseInstance.clearAllTables();
    }

    public abstract ItemDao itemDao();
}
