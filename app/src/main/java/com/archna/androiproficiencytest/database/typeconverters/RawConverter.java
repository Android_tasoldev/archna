package com.archna.androiproficiencytest.database.typeconverters;

import androidx.room.TypeConverter;

import com.archna.androiproficiencytest.database.entity.RawEntity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class RawConverter implements Serializable {

    @TypeConverter // note this annotation
    public String fromRawItemList(List<RawEntity> optionValues) {
        if (optionValues == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<RawEntity>>() {
        }.getType();
        return gson.toJson(optionValues, type);
    }

    @TypeConverter // note this annotation
    public List<RawEntity> toRawItemList(String optionValuesString) {
        if (optionValuesString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<RawEntity>>() {
        }.getType();
        return gson.fromJson(optionValuesString, type);
    }
}