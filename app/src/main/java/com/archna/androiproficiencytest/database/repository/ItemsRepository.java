package com.archna.androiproficiencytest.database.repository;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.archna.androiproficiencytest.database.AppDatabase;
import com.archna.androiproficiencytest.database.entity.ItemEntity;
import com.archna.androiproficiencytest.util.interfaces.DatabaseCallBack;

import java.util.concurrent.Executors;

public class ItemsRepository {

    private AppDatabase _appDatabase;
    private LiveData<ItemEntity> _liveItemData = new MutableLiveData<>();

    // default constructor
    public ItemsRepository(Context context) {
        this._appDatabase = AppDatabase.getInstance(context);
        _liveItemData = _appDatabase.itemDao().getLiveItems();
    }

    public LiveData<ItemEntity> getLiveItemData() {
        return _liveItemData;
    }

    // Insert item in to database
    public void insertItem(ItemEntity itemEntity, DatabaseCallBack<Long> callBack) {
        new MakeDBCallForInsert(_appDatabase, callBack, itemEntity).execute();
    }

    // Delete All items from database and Add new items
    public void deleteOldDataAndInsertNewData(ItemEntity itemEntity, DatabaseCallBack<Long> callBack) {
        new MakeDBCallForDeleteAndInsert(_appDatabase, callBack, itemEntity).execute();
    }

    public void getItemCount(DatabaseCallBack<Integer> callBack) {
        Executors.newSingleThreadExecutor().execute(() -> {
            try {
                callBack.onSuccess(_appDatabase.itemDao().getCount());
            } catch (Exception e) {
                e.printStackTrace();
                callBack.onFailure();
            }
        });
    }

    // asyncTask for delete and insert operation
    private static class MakeDBCallForDeleteAndInsert extends AsyncTask<Void, Void, Long> {

        private DatabaseCallBack<Long> callBack;
        private AppDatabase _appDatabase;
        private ItemEntity itemEntity;

        MakeDBCallForDeleteAndInsert(AppDatabase _appDatabase, DatabaseCallBack<Long> callBack, ItemEntity itemEntity) {
            this.callBack = callBack;
            this._appDatabase = _appDatabase;
            this.itemEntity = itemEntity;
        }

        @Override
        protected Long doInBackground(Void... voids) {
            _appDatabase.itemDao().deleteAllItems();
            return _appDatabase.itemDao().insertItem(itemEntity);
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            if (callBack != null)
                if (aLong != null && aLong > 0)
                    callBack.onSuccess(aLong);
                else
                    callBack.onFailure();
        }
    }

    // Common asyncTask for  db operation
    private static class MakeDBCallForInsert extends AsyncTask<Void, Void, Long> {

        private DatabaseCallBack<Long> callBack;
        private AppDatabase _appDatabase;
        private ItemEntity itemEntity;

        MakeDBCallForInsert(AppDatabase _appDatabase, DatabaseCallBack<Long> callBack, ItemEntity itemEntity) {
            this.callBack = callBack;
            this._appDatabase = _appDatabase;
            this.itemEntity = itemEntity;
        }

        @Override
        protected Long doInBackground(Void... voids) {
            return _appDatabase.itemDao().insertItem(itemEntity);
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            if (callBack != null)
                if (aLong != null && aLong > 0)
                    callBack.onSuccess(aLong);
                else
                    callBack.onFailure();
        }
    }
}