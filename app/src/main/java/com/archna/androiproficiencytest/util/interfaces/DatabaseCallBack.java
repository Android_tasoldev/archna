package com.archna.androiproficiencytest.util.interfaces;

public interface DatabaseCallBack<T> {

    void onSuccess(T item);

    void onFailure();
}
