package com.archna.androiproficiencytest.util;

import androidx.databinding.BindingAdapter;

import com.facebook.drawee.view.SimpleDraweeView;

public class Utility {

    public static boolean isValidString(String string) {
        return string != null && !string.equalsIgnoreCase("") && !string.equalsIgnoreCase("null");
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(SimpleDraweeView view, String imageUrl) {
        view.setImageURI(imageUrl);
    }

}
