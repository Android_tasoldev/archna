package com.archna.androiproficiencytest.util.interfaces;

public interface OnItemClick<T> {
    void onCLick(T object, int position);
}
