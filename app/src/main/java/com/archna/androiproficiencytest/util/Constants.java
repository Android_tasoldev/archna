package com.archna.androiproficiencytest.util;

public class Constants {

    // Database Constants //
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ANDROID_PROFICIENCY_TEST_DB";

    // Api Constants //
    public static final int API_GET_ITEM = 1;

    // View Constants

    public static final String NO_DATA = "no_data";
    public static final String CONTENT = "content";
}
